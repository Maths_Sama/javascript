# ![binaire](images/binaire.png) Représentation des nombres entiers naturels en informatique

Quelques soient les moyens de calcul informatiques mis en œuvre, il appartiennent au domaine du discret et du fini. Ainsi il ne peuvent manipuler
et représenter qu’un nombre fini (même s’il est très grand) d’informations.
En revanche, en mathématiques nous utilisons couramment les concepts d’exactitude et d’infini. 
L’utilisation de l’outil informatique impose donc d’adopter
des représentations fines pour les quantités numériques (entier, réels, ...) et introduit forcément un erreur due à ces représentations.


## ![binairelogo](images/binaire_logo.png) Représentation des entiers en base 10

Le développement en base 10, ou écriture décimale, est le système de numération le plus répandu actuellement.
L’utilisation de la base 10 provient très probablement du fait que nous ayons 10 doigts.

Il n’en a pas toujours été ainsi et vous trouverez ci dessous une liste, non exhaustive, des différents systèmes de numération utilisé à travers les époques
et les peuples :
- le système binaire (base 2) utilisé en d’Amérique du Sud et en Océanie
- le système quinaire (base 5) était utilisé parmi les premières civilisations, et jusqu’au XXe siècle par des peuples africains, mais aussi,
partiellement, dans les notations romaine et maya
- le système octal (base 8) utilisé au Mexique.
- le système décimal (base 10) a été utilisé par de nombreuses civilisations, comme les Chinois dès les premiers temps, et, probablement, les
Proto-indo-européens. Aujourd’hui, il est de loin le plus répandu.
- le système duodécimal (base 12) utilisé au Népal. On le retrouve, à cause de ses avantages en matière de divisibilité (par 2, 3, 4, 6), pour un certain nombre de monnaies et d’unités de compte courantes en Europe au Moyen Âge, partiellement dans les pays anglo-saxons dans le système d’unité impérial, et dans le commerce. Notre façon de compter les mois et les heures est un vestige de son utilisation.
- le système sexagésimal (base 60) était utilisé pour la numération babylonienne, ainsi que par les Indiens et les Arabes en trigonométrie. Il sert actuellement dans la mesure du temps et des angles.

##  ![binairelogo](images/binaire_logo.png) Représentation des entiers en base 2

Le binaire, ou base 2, est la base naturelle de représentation d’un nombre en informatique. 
En effet, dans un ordinateur, les informations (donc aussi les nombres) sont stockées sous forme de suites de bit (binary digit), chaque bit valant 0 ou 1. Le bit est donc l’unité élémentaire d’information en informatique.

### Vidéo à visionner : **Représentation des entiers naturels en binaire | Ronan Boulic**

[![image](images/Unitag_QRCode_1568010231306.png)](https://www.youtube.com/embed/a5gLSc0tbjI)

## ![binaire_titre](images/binaire_logo_titre.png) Méthode : du binaire au décimal

Dans ce sens le passage d’une écriture à l’autre est immédiat : il suffit d’effectuer les opérations du développement binaire :

**$`11011_2 = \left(1\times 2^4 + 1\times 2^3 + 0\times 2^2 + 1\times 2^1 + 1\times 2^0 \right)_{10} = 27_{10}`$**

|$`2^{12}`$|$`2^{11}`$|$`2^{10}`$|$`2^9`$|$`2^8`$|$`2^7`$|$`2^6`$|$`2^5`$|$`2^4`$|$`2^3`$|$`2^2`$|$`2^1`$| $`2^0`$ |
|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
|` `|` `|` `|` `|` `|` `|` `|` `| 1 | 1 | 0 | 1 | 1 |
|` `|` `|` `|` `|` `|` `|` `|` `|$`1\times~2^4`$|$`1\times 2^3`$|$`0\times 2^2`$|$`1\times 2^1`$|$`1\times 2^0`$|

## ![binaire_titre](images/binaire_logo_titre.png) Méthode : du décimal au binaire

Le passage du décimal au binaire repose sur l’application itérative suivante :

> Le digit le moins significatif du développement d'un entier **_`n`_** en base 2, est égal au reste de la division de **_`n`_** par 2.


**$`67 = 2\times 33 + 1 \longrightarrow 1\textrm{(bit de poids faible)} \longrightarrow 1\times 2^0`$**

**$`33 = 2\times 16 + 1 \longrightarrow 1 \longrightarrow 1\times 2^1`$**

**$`16 = 2\times 8 + 0 \longrightarrow 0 \longrightarrow 0\times 2^2`$**

**$`8 = 2\times 4 + 0 \longrightarrow 0 \longrightarrow 0\times 2^3`$**

**$`4 = 2\times 2 + 0 \longrightarrow 0 \longrightarrow 0\times 2^4`$**

**$`2 = 2\times 1 + 0 \longrightarrow 0 \longrightarrow 0\times 2^5`$**

**$`1 = 2\times 0 + 1 \longrightarrow 1\textrm{(bit de poids fort)} \longrightarrow 1\times 2^6`$**

**$`67_{10} = 1000011_2`$**

##  ![binairelogo](images/binaire_logo.png) Représentation des entiers en base 16


### Vidéo à visionner : **L'information en hexadécimal | Ronan Boulic**
[![image](images/Unitag_QRCode_1568010359909.png)](https://www.youtube.com/embed/rX3Lwn7rAAY)

L'écriture hexadécimale, ou base 16, est une autre base utilisée fréquemment en informatique. L’utilisation de l’hexadécimale permet de représenter les nombres de
manière plus compacte qu’en binaire tout en assurant un passage binaire ↔ hexadécimale très rapide.

Pour écrire un nombre en hexadécimal il nous font donc disposer de 16 symboles. Pour les dix premiers symboles on utilise les 10 chiffres 0, 1, · · · , 9 et
pour les six derniers on utilise les six premières lettres de l’alphabet A, B, C, D, E et F.

|Symbole|0|1|2|3|4|5|6|7|8|9|A|B|C|D|E|F|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Valeur|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|



## ![binaire_titre](images/binaire_logo_titre.png) Méthode : Passage du binaire à l’hexadécimal et inversement

Pour passer de l’écriture hexadécimale à l’écriture binaire il suffit de remplacer chaque digit hexadécimal par sa représentation binaire : **$`1BF_{16} = 0001\;1011\;1111_2`$**

Dans le sens inverse il suffit de découper l’écriture binaire en groupe de 4 bits, quitte à rajouter des 0 à gauche si le nombre total de bits n’est divisible par 4,
et de remplacer chaque groupe de 4 bits par sa représentation hexadécimale :  **$`1011011_2 = 0101\;1011_2 = 5B_{16}`$**

## ![binaire_titre](images/binaire_logo_titre.png) Méthode : Passage de l’hexadécimal au décimal

On procède de la même manière que pour le binaire, en effectuant les opérations du développement hexadécimal tout en tenant comptes des valeurs des différents
symboles données dans le tableau ci-dessus : **$`1BF_{16}= \left(1 \times 16^2 + 11 \times 16^1 + 15 \times 16^0 \right)_{10}= 44710_{10}`$**

## ![binaire_titre](images/binaire_logo_titre.png) Méthode : Passage du décimal à l'héxadécimal

Même si l’on peut procéder par divisions successives par 16 en pratique on ne le fait pas. En effet la division par 16 est fastidieuse alors que celle par 2
est immédiate et il est très facile de passer du binaire à l’hexadécimal comme nous l’avons vu ci-dessus. Donc pour passer du décimal à l’hexadécimal on passe par la représentation binaire que l’on écrit ensuite en hexadécimal : **$`43_{10} = 101011_2 = 00101011_2 = 2B_{16}`$**

##  ![binairelogo](images/binaire_logo.png) Codage des entiers naturels en informatique

La représentation machines des entiers naturel repose naturellement sur leur écriture binaire. 
L’ensemble des entiers que l’on peut ainsi représenter dépend uniquement du nombre de bits utilisés pour le codage. 
De manière générale si l’on utilise $`n`$ bits pour le codage d’un entier on peut représenter tous les entiers de $`0`$ à $`2n −1`$.

**Dans la plupart des cas** les entiers naturels sont codés sur :
- 16 bits (2 octets) permettant de représenter les entiers de l’intervalle
$`[0, \dots , 65 535]`$
- 32 bits (4 octets) permettant de représenter les entiers de l’intervalle
$`[0, \dots , 4 294 967 295]`$
- 64 bits (8 octets) permettant de représenter les entiers de l’intervalle
$`[0, \dots , 18 446 744 073 709 551 615]`$

### Vidéo à visionner : **Représentation de l'information en binaire | Ronan Boulic**
[![image](images/Unitag_QRCode_1568010446140.png)](https://www.youtube.com/embed/YWkZ1VpnZ-w)

-----
##   ![binairelogo](images/binaire_logo.png) Exercices

1. Convertir $`128_{10}`$ en binaire et en hexadécimal.
2. Convertir $`517_{10}`$ en binaire et en hexadécimal.
3. Convertir $`110001011101_2`$ en décimal et en hexadécimal.
4. Convertir $`571_{10}`$ en base 16.
5. Convertir en décimal $`37FD_{16}`$ et $`2C0_{16}`$.
6. Effectuer l’addition $`1110 1001_2 + 11 1001_2`$
7. Effectuer l’addition $`1111 1111_2 + 1_2`$ 
8. Effectuer l’addition $`1110_2 + 1010_2`$
9. Effectuer l’addition $`4AF_{16} + B25_{16}`$
10. Effectuer l’addition $`FF_{16} + FF_{16}`$
11. Sur 1 octets quel est l'intervalle des entiers naturels représentables?
12. Combiens de ``bits`` sont nécessaires pour représenter les mois de l'année?

-----------------------
##  ![projet](images/projet.png) Réaliser un module de conversion des entiers naturels.
-----------------------

## ![javalogo](../images/javalogo.png) Création de la fonction ``binDeci(binaire)``

```javascript
/**
 * Obtenir l'écriture décimale d'un entier naturel en écriture binaire.
 * 
 * @example
 * // returns '2'
 * binDeci('10');
 * @example
 * // returns '5'
 * binDeci('101');
 * @author DarkSATHI
 * @param {string} [binaire = "0"] L'écriture binaire d'un entier naturel.
 * @return {string} L'écriture décimale d'un entier naturel.
 * @version 0.1
 */

function binDeci(binaire = "0"){
    decimal = null
    for(i=0; i<binaire.length; i++){
       decimal += Math.pow(2, binaire.length-1-i)*binaire[i]
    }
    return String(decimal)
}

let binaire = window.prompt("Donner un nombre en écriture binaire")
alert(binDeci(binaire))
```
Tester ce code dans [JSFiddle](https://jsfiddle.net/)

**Il est trés important de documenter son code : On utilisera [JSDoc](https://devdocs.io/jsdoc/)**

|**Quelques Tag**|**Description**|
|:--:|:--:|
|@author|	Nom ou pseudo du développeur|
| @constructor|	Marque une fonction en tant que constructeur|
| @deprecated|	Marque une méthode obsolète|
| @throws|	Documente une exception lancée par une méthode|
| @exception|	Synonyme pour @throws|
|@exports|	Identifie un membre exporté par un module|
| @param|	Documente les paramètres d'une méthode; le type du paramètre peut être ajouté par des accolades|
| @private	|Signifie qu'un membre est privé|
| @return|	Documente le retour d'une fonction/méthode|
| @returns|	Synonyme de @return|
| @see|	Documente une association vers un autre objet|
| @this	|Détermine le type de l'objet duquel le mot clé "this" réfère dans une fonction (window par exemple)|
| @version|	Donne la version de la bibliothèque|
