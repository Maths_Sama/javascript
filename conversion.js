/**
 * @fileOverview : Module de foction de conversion Binaire | Hexadécimale | Décimale |
 * Obtenir l'écriture décimale d'un entier naturel en écriture binaire.
 * 
 * @example
 * // returns '2'
 * binDeci('10');
 * @example
 * // returns '5'
 * binDeci('101');
 * @author DarkSATHI
 * @param {string} [binaire = "0"] L'écriture binaire d'un entier naturel.
 * @return {string} L'écriture décimale d'un entier naturel.
 * @version 0.1
 */

function binDeci(binaire = "0"){
    decimal = null
    for(i=0; i<binaire.length; i++){
       decimal += Math.pow(2, binaire.length-1-i)*binaire[i]
    }
    return String(decimal)
}

let binaire = window.prompt("Donner un nombre en écriture binaire")
alert(binDeci(binaire))